declare module "*.md" {
    import { ComponentType } from "react";

    export type TableOfContents = {
        level: string;
        content: string;
    }[];

    export const html: string;
    export const ReactComponent: ComponentType;
    export const attributes: Record<string, never>;
    export const toc: TableOfContents;
}
