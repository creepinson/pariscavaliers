import React from "react";
import {
    ChakraProvider,
    ColorModeProvider,
    Heading,
    Flex,
    Link,
} from "@chakra-ui/react";
import { render } from "react-dom";
import "./index.scss";
import { theme } from "./lib/theme";
import { Link as WLink, Route } from "wouter";
import { AboutPage } from "./pages/about";
import { HomePage } from "./pages/home";
import { BlogPage } from "./pages/blog";

render(
    <ChakraProvider theme={theme}>
        <ColorModeProvider options={theme.config}>
            {/* Header */}
            <Flex
                flexDirection="column"
                p="15px"
                bg="gray.700"
                alignItems="center"
                justifyContent="center"
                mb="15px"
            >
                <Heading as="h1" fontSize="32">
                    <Link as={WLink} to="/">
                        The Paris Family Cavaliers
                    </Link>
                </Heading>
                {/* Navigation */}
                <Flex
                    flexDirection="row"
                    alignItems="center"
                    justifyContent="center"
                >
                    <Link as={WLink} to="/about" mr="10px" fontSize="20">
                        About Us
                    </Link>
                    <Link as={WLink} to="/blog" mr="10px" fontSize="20">
                        Blog
                    </Link>
                </Flex>
            </Flex>
            {/* Routes */}
            <Flex
                flexDirection="column"
                p="15px"
                alignItems="center"
                justifyContent="center"
            >
                <Route path="/">
                    <HomePage />
                </Route>
                <Route path="/about">
                    <AboutPage />
                </Route>
                <Route path="/blog">
                    <BlogPage />
                </Route>
            </Flex>
        </ColorModeProvider>
    </ChakraProvider>,
    document.getElementById("app")!
);
