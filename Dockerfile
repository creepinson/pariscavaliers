# Stage 1 (Build app)
FROM creepinson/alpine-pnpm as builder

WORKDIR /app

COPY package*.json /app/
RUN pnpm i
COPY ./ /app/

ENV API_URL http://api:6969

RUN pnpm run build

# Stage 2 (Serve app with nginx)

FROM nginx:alpine
COPY --from=build-stage /app/dist/ /usr/share/nginx/html
